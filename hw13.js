let link = document.getElementById('theLink');

function save () {

    if (window.localStorage.getItem('themeStyle') === 'css/color1.css') {
        link.href = 'css/color2.css';

         window.localStorage.setItem('themeStyle', 'css/color2.css');
    } else {
        link.href = 'css/color1.css';
        window.localStorage.setItem('themeStyle', 'css/color1.css');

    }
}
document.addEventListener("DOMContentLoaded", function () {
    if (window.localStorage.getItem('themeStyle') === null) {
        link.href = 'css/color1.css';
        window.localStorage.setItem('themeStyle', 'css/color1.css');
    }
    link.href = window.localStorage.getItem('themeStyle');
});
document.getElementById('switch').addEventListener('click', save );


window.onunload = function (event) { //при закритии страницы данные сохраняются
    window.localStorage.getItem('themeStyle');
};